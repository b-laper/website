import Background from "./components/Background";
import Content from "./components/Content";
import Navbar from "./components/Navbar";

const App = (): JSX.Element => {
  return (
    <div>
      <Navbar />
      <Background />
      <Content />
    </div>
  );
};

export default App;
