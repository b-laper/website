import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Contact from "./components/Contact";
import { Provider } from "react-redux";
import store from "./state/store";
import Gallery from "./components/Gallery";
import Projects from "./components/Projects";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={store}>
    <BrowserRouter basename="/">
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/Realizacje" element={<Projects />} />
        <Route path="/Projects" element={<Projects />} />
        <Route path="/Galeria" element={<Gallery />} />
        <Route path="/Gallery" element={<Gallery />} />
        <Route path="/Kontakt" element={<Contact />} />
        <Route path="/Contact" element={<Contact />} />
      </Routes>
    </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
