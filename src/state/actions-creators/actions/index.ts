export interface IChangeLanguagePayload {
  language: string;
}
