import { createAction } from "@reduxjs/toolkit";
import { ActionTypes } from "../action-types";
import { IChangeLanguagePayload } from "./actions";

export const changeLanguage = createAction(
  ActionTypes.CHANGE_LANGUAGE,
  ({ language }: IChangeLanguagePayload) => {
    return {
      payload: { language },
    };
  }
);
