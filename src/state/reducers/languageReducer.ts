import { createReducer } from "@reduxjs/toolkit";
import { changeLanguage } from "../actions-creators";

export interface ILanguageState {
  language: "pl" | "uk";
}

if (!window.localStorage) {
  console.log("Your browser does not support local storage");
}

const initialState = window.localStorage.getItem("language") || "pl";

export const languageReducer = createReducer(initialState, (builder) => {
  builder.addCase(changeLanguage, (state, { payload: { language } }) => {
    state = language;
    window.localStorage.setItem("language", language);
    return state;
  });
});
