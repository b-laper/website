import "./Content.css";
import { useTypedSelector } from "../hooks/use-typed-selector";
import Typewriter from "typewriter-effect";

const Content = () => {
  const language = useTypedSelector((state) => state.language);

  const renderContentDetails = (arr: string[]) => {
    return arr.map((site: string, index: number) => (
      <li key={index}>{site}</li>
    ));
  };

  const headerMessage = "Nazwa firmy";

  const contentPl = [
    "Pomiary elektryczne",
    "Inteligentne domy",
    "Automatyka przemysłowa",
    "Automatyka jachtowa",
    "Systemy alarmowe",
    "Instalacje elektryczne",
    "Serwis elektryczny",
    "Fotowoltaika",
    " Turbiny wiatrowe",
    "Zasilanie awaryjne",
  ];

  const contentUk = [
    "Electrical Measurements",
    "Smart homes",
    "Industrial automation",
    "Yacht automation",
    "Alarm systems",
    "Electrical installations",
    "Electrical service",
    "Photovoltaics",
    " Wind turbines",
    "Emergency power supply",
  ];

  return (
    <>
      <header>
        <h1 className="title">
          <Typewriter
            onInit={(typewriter) => {
              typewriter.typeString(headerMessage).pauseFor(2500).start();
            }}
          />
        </h1>
      </header>
      <section>
        <ul>
          {language === "pl"
            ? renderContentDetails(contentPl)
            : renderContentDetails(contentUk)}
        </ul>
      </section>
    </>
  );
};

export default Content;
