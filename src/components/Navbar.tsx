import "./Navbar.css";
import { Link } from "react-router-dom";
import logo from "../img/logo.png";
import icon from "../img/menu-icon.png";
import pl from "../img/pl-icon.png";
import uk from "../img/uk-icon.png";
import { useDispatch } from "react-redux";
import { changeLanguage } from "../state/actions-creators";
import { useTypedSelector } from "../hooks/use-typed-selector";
import { useState } from "react";

const Navbar = (): JSX.Element => {
  const [toggleMenu, setToggleMenu] = useState<boolean>(false);
  const navSites = ["Realizacje", "Galeria", "Kontakt"];
  const navSitesEng = ["Projects", "Gallery", "Contact"];
  const dispatch = useDispatch();
  const language = useTypedSelector((state) => state.language);

  const navLang = (arr: string[]) => {
    return arr.map((site: string, index: number) => (
      <Link key={index} className="links" to={`/${site}`}>
        <li>{site}</li>
      </Link>
    ));
  };

  const renderChangeLanguage = (): JSX.Element => {
    return (
      <div className="flags">
        <img
          onClick={() => dispatch(changeLanguage({ language: "pl" }))}
          className="flag-icon"
          src={pl}
          alt="pl-flag"
        />
        <img
          onClick={() => dispatch(changeLanguage({ language: "uk" }))}
          className="flag-icon"
          src={uk}
          alt="uk-flag"
        />
      </div>
    );
  };

  const viewMenu = () => {
    return (
      <div className="mobile-menu">
        {renderChangeLanguage()}
        {language === "pl" ? navLang(navSites) : navLang(navSitesEng)}
        <span onClick={() => setToggleMenu(false)} className="close">
          x
        </span>
        <Link to="/">
          <img className="mobile-logo" src={logo} alt="mobile-logo" />
        </Link>
      </div>
    );
  };
  return (
    <div className="background">
      <nav>
        <div className="nav-links">
          <Link className="links" to="/">
            <img className="logo" src={logo} alt="logo" />
          </Link>
        </div>
        <span className="name">
          <img
            className="menu-icon"
            onClick={() => setToggleMenu(!toggleMenu)}
            src={icon}
            alt="menu-icon"
          />
        </span>
        <div>
          <ul className="nav-sites">
            {language === "pl" ? navLang(navSites) : navLang(navSitesEng)}

            <div className="lang">
              <li className="language-li">Język/Language</li>
              {renderChangeLanguage()}
            </div>
          </ul>
        </div>
      </nav>
      <div>{toggleMenu && viewMenu()}</div>
    </div>
  );
};

export default Navbar;
