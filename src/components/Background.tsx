import "./Background.css";
import image from "../img/background.jpg";

interface IBackgroundProps {
  img?: any;
}

const Background = ({ img }: IBackgroundProps): JSX.Element => {
  return (
    <div>
      <img className="bgc-img" src={img || image} alt="background" />
    </div>
  );
};

export default Background;
