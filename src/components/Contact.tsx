import "./Contact.css";
import Background from "./Background";
import Navbar from "./Navbar";
import image from "../img/background3.png";
import { useTypedSelector } from "../hooks/use-typed-selector";
import Typewriter from "typewriter-effect";

const Contact = (): JSX.Element => {
  const state = useTypedSelector((state) => state.language);

  const headerDetails =
    state === "pl" ? "Zamów usługi..." : "Ask for services...";

  return (
    <div className="contact">
      <Navbar />
      <Background img={image} />
      <div>
        <h3 className="contact-header">
          <Typewriter
            options={{
              strings: [headerDetails],
              autoStart: true,
              loop: true,
            }}
          />
        </h3>
      </div>
      <div className="contact-content">
        <h3>+ 48 531 826 737 </h3>
        <h3>kontakt@elekoscierzyna.pl</h3>
      </div>
    </div>
  );
};

export default Contact;
