import Background from "./Background";
import Navbar from "./Navbar";

const Projects = (): JSX.Element => {
  return (
    <div>
      <Navbar />
      <Background />
      <h1>Projects</h1>
    </div>
  );
};

export default Projects;
