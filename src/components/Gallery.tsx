import "./Gallery.css";
import Background from "./Background";
import Navbar from "./Navbar";
import image from "../img/background3.png";
import ImageGallery from "react-image-gallery";

const Gallery = (): JSX.Element => {
  const images = [
    {
      original:
        "https://smoglab.pl/wp-content/uploads/2019/06/czarnobyl_skutki-750x375.jpg.webp",
    },
    {
      original:
        "https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fwww.ppoz.pl%2Fassets%2Fpics%2Faktualnosci%2FCzarnobyl%2Fimage001.jpg&sp=1660928923T84c5171be8ceb2d27c729597015cd5b4a6eae2dabc8c5ba884c83c6adb67891e",
    },
    {
      original:
        "https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fwww.intro.media%2Fimages%2Fwww%2Fczarnobyl5.jpg&sp=1660928923T3b362c5056e42dc30bc105c86a78266cc22143f06746c1f72f6bb2687863e95c",
    },
  ];

  return (
    <>
      <div>
        <Navbar />
        <Background img={image} />
      </div>
      <div className="gallery">
        <ImageGallery items={images} showThumbnails={false} />
      </div>
    </>
  );
};

export default Gallery;
